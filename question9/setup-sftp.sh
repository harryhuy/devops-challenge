#!/bin/bash

# Assembled by Harry
#
# Referrence: https://www.techrepublic.com/article/how-to-set-up-an-sftp-server-on-linux/
#
# Requirements: SSH is installed and allow remote connection.
#
# Tested on Ubuntu 20.04

# Define variables
SFTP_DATA_DIRECTORY=/var/sftp/data
SFTP_USERS_GROUP=sftp_users
USER_HOME_DIRECTORY=/uploads

# Create SFTP data direcctory
sudo mkdir -p -m 750 $SFTP_DATA_DIRECTORY
echo 'Created SFTP Data Directory.'

# Create SFTP users' group
sudo groupadd $SFTP_USERS_GROUP
echo 'Created SFTP USers Group'

# Users is provided in a text file with the format is:
# <username 1>:<hashed password 1>
# <username 2>:<hashed password 2>
# ...
# Hashed password is the Unix hashed password can be produce using Python Crypt standard library:
# https://docs.python.org/3/library/crypt.html
for LINE in $( cat ./users.txt ); do
    U=(${LINE//:/ })
    # Create sftp user with chrooted home directory, nologin shell, and have SFTP users group membership
    sudo useradd -g sftp_users -s /sbin/nologin -d $USER_HOME_DIRECTORY -p ${U[1]} ${U[0]}
    echo "User ${U[0]} has been added."

    # Create user's data directory
    sudo mkdir -p -m 750 $SFTP_DATA_DIRECTORY/${U[0]}/${USER_HOME_DIRECTORY}
    sudo chown root:${SFTP_USERS_GROUP} ${SFTP_DATA_DIRECTORY}/${U[0]}
    sudo chown ${U[0]}:${SFTP_USERS_GROUP} ${SFTP_DATA_DIRECTORY}/${U[0]}/${USER_HOME_DIRECTORY}
    echo "Created users' directories."
done

# Replace Subsystem "/usr/lib/openssh/sftp-server" with "internal-sftp" to support ChrootDirectory
# and enable loging for SFTP, the logs will be placed in "/var/log/auth.log".
sudo sed -i '/Subsystem\s*sftp.*/c\Subsystem sftp internal-sftp -f AUTH -l VERBOSE' /etc/ssh/sshd_config
echo 'Altered SSH SFTP Subsystem.'

# Configure ChrootDirectory and restrict the remote shell.
# SFTP_USERS_CONFIG='Match Group sftp_users\nChrootDirectory\n/var/sftp/data/%u\nForceCommand internal-sftp'
sudo sed -i '$a\
Match Group sftp_users\
ChrootDirectory /var/sftp/data/%u\
ForceCommand internal-sftp' /etc/ssh/sshd_config
echo 'Configure ChrootDirectory.'

sudo systemctl restart sshd
echo 'Restarted sshd.'
