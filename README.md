## Question 1

> How would your ideal workstation setup be like? Software-wise, configuration-wise and, optionally, hardware-wise. Explain why you would set it up that way.

Obviously, it depends on demand. The engineer workstations would have different requirements than the designer workstations, the developer workstations, and so on, especially on the installed applications and computing power. But there are common matters that every organization has to care about. The ability to manage and maintain all workstations centrally, the security, and the investment cost.

There are many solutions the market can help us to manage the endpoints centrally, for example, Active Directory, ManageEngine, SolarWind... they are becoming to support more and more kinds of endpoints, but the fact is the less differential in the system, the easier to maintain. Try to limit the number of hardware, OS, and software brands as less as possible.

For security, the practice is much simple than what people usually think. With the presence of the centralized management tool, the latest hardware, OS, and software installed, security flaws will be eliminated significantly.

Finally, how about the cost of investing software and hardware for our machines. If we have unlimited resources to build the system, just simply buy the highest one on the market. If not, we must dive on each manufacturer's product list to find out which one matches our needs.

## Question 2

> Containers or virtual machines for application deployments? And why?

If the application is a collection of services has been designed to provide a consistent development and automated deployment across different environments (on-premise, cloud, hybrid). Then the lightweight and independent container will help to isolate the application's components and speed up the development process.

If the application is built on the traditional monolithic architecture and the portability isn't so important, then the virtual machine can come in. Virtual machines are useful when the application provisions infrastructural resources such as network, proxy..., and when we need to create the boundary to protect the application holds sensitive data.

## Question 3

> Name some good CI/CD tools that you know. Which one is your favorite, and why?

Let's compare Jenkins and Gitlab.

Jenkins is one of the most popular automation CI/CD tool. With so many available plugins, it is able to support building, testing, and deploying automatically any project. Jenkins X is a sub-project has been developed to provide the integration with K8s, Helm,... to offer a prescriptive CI/CD pipeline. However, its components pairing is very complicated.

In contrast, Gitlab already provides more than what Jenkins is envolving to, with a fully integrated application for the entire DevOps lifecycle, planning capability, SCM, packaging, release, configuration, and monitoring.

I prefer Gitlab because Jenkins is old.

## Question 4

> Name and shortly explain one or some git branching models that you know (be they famous or
custom). If multiple models are mentioned, a comparison is a big plus.

Gitflow maybe the branching model that every DevOps engineer should know. It suggests a master branch and a seperate develop branch, as well as supporting branches for features, releases, and hotfixes. The development happends on the develop branch, moves to release branch, and finally merged into the master branch.

The `master` branch is considered to be the main branch where the source code always reflects a production-ready state, the `develop` branch is where the latest delivered changes come in, the `feature` branches are used to develop new features, `release` branch support preparation of a new production release, and `hotfix` is for fixing critical bugs in a production version.

Gitflow is ideally suited for projects that have a scheduled release cycle. This workflow doesn’t add any new concepts or commands beyond what’s required for the Feature Branch Workflow. Instead, it assigns very specific roles to different branches and defines how and when they should interact. In addition to feature branches, it uses individual branches for preparing, maintaining, and recording releases. Of course, you also get to leverage all the benefits of the Feature Branch Workflow: pull requests, isolated experiments, and more efficient collaboration.

Gitlow graphical model:

![Gitflow model](https://nvie.com/img/git-model@2x.png)

Gitflow is a well-defined standard, but its complexity introduces two problems. The first problem is that developers must use the `develop` branch and not `master`. `master` is reserved for code that is released to production. It is a convention to call your default branch `master` and to mostly branch from and `merge` to this. Since most tools automatically use the master branch as the default, it is annoying to have to switch to another branch.

The second problem of Gitflow is the complexity introduced by the `hotfix` and `release` branches. These branches can be a good idea for some organizations but are overkill for the vast majority of them. Nowadays, most organizations practice continuous delivery, which means that your default branch can be deployed. Continuous delivery removes the need for `hotfix` and `release` branches, including all the ceremony they introduce. An example of this ceremony is the merging back of release branches. Though specialized tools do exist to solve this, they require documentation and add complexity.

Referrence:

https://docs.gitlab.com/ee/topics/gitlab_flow.html

https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

## Question 5

> Managed or self-hosted Kubernetes clusters? Why? Pros and cons?

With the self-hosted Kubernetes, we have more flexibility over our cluster but it requires system administrator's skill and time to manage and maintain. On the other hand, managed cluster certainly cuts the overhead of deploying and maintaining.

## Question 6

> Write a bash script to find the Monday of the current week.

> Example:

> * Today: Thursday, July 2, 2020

> * The expected Monday: Monday, June 29, 2020

Answer in ./question6 directory

# Question 7

> Write Terraform code to provision an AWS VPC with a public subnet and a private subnet. Using community predefined modules or writing your own code are equally acceptable, but please explain your code.

Answer in ./question7 directory

# Question 8

> Write an Ansible playbook to configure an SFTP server based on OpenSSH. It is acceptable to use community code, but please explain what you have and why you write it that way. For the purpose of evaluating your code, please clarify your distro and its version.

Answer in ./question8 directory

# Question 9

> Write a Bash script for the same task in the previous Ansible question. It is acceptable for this script to only be of one-time use against a fresh machine (no need to execute correctly from the 2nd time running).

Answer in ./question9 directory

# Question 10

> Write your own Dockerfile to provision a Jenkins instance, with the final result being as small as possible. Please explain your Dockerfile. Commands can be collected from different sources, but a publicly available Dockerfile is not considered acceptable.

Answer in ./question10 directory
