# Assembled by Harry

# To create a smallest output, Ubuntu base image would be the good starting point.
# Referrence: https://crunchtools.com/comparison-linux-container-images/
FROM ubuntu:20.04

LABEL Maintainer Harry Huy (iam@harryhuy.net)

# Suppress the APT's warning and apply default settings for the interactive config 
ARG DEBIAN_FRONTEND=noninteractive 

# Install Curl, Git, & Mini JRE...
RUN apt-get update && apt-get upgrade -y && apt-get install -y curl git openjdk-11-jre-headless

# Clean retrieved packages
RUN apt-get clean

# Settings for the build process
ARG USER=jenkins
ARG GROUP=jenkins
ARG UID=1000
ARG GID=1000
ARG JENKINS_VERSION=latest
ARG JENKINS_SHA256=e88642a2b52fc26ffa4425a0aba65163f083d770e5ef7182b2e32de41ff33981

# Setting for Jenkins
ENV JENKINS_HOME=/var/jenkins
ENV JENKINS_HTTP_PORT=8080

# Download Jenkins
RUN curl -L http://mirrors.jenkins.io/war-stable/${JENKINS_VERSION}/jenkins.war -o /tmp/jenkins.war \
    && echo "${JENKINS_SHA256} /tmp/jenkins.war" | sha256sum -c -

# Create Jenkins' home directory
RUN mkdir -p $JENKINS_HOME && chown ${UID}:${GID} $JENKINS_HOME && groupadd -g $GID $GROUP \
    && useradd -d "${JENKINS_HOME}" -u $UID -g $GID -m -s /bin/bash $USER

# Public web portal port
EXPOSE $JENKINS_HTTP_PORT

# Switch to Jenkins user
USER $USER

# Start Jenkins
ENTRYPOINT [ "java", "-jar", "/tmp/jenkins.war", "httpPort=${JENKINS_HTTP_PORT}" ]
