# VPC Overview

## Network topology

![VPC Network diagram](https://docs.aws.amazon.com/vpc/latest/userguide/images/nat-gateway-diagram.png)

## Referrences

https://github.com/terraform-aws-modules/terraform-aws-vpc/tree/master/examples/simple-vpc
