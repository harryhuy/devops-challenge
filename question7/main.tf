provider "aws" {
  region = "ap-southeast-1"
}

# Create a new default Security group
data "aws_security_group" "default" {
  vpc_id = module.vpc.vpc_id
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "testing-vpc"

  cidr = "10.0.0.0/16"
  
  # Availability zone
  azs = ["apse1-az2"]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.0.0/24"]

  # Many new applications and services nowaday starting use IPv6, so we should enable it
  enable_ipv6 = true

  # Private subnet will send out its internet traffic via the NAT Gateway
  enable_nat_gateway = true
  # Create only one NAT Gateway for the private subnet
  single_nat_gateway = true

  # Tags assinging
  tags = {
    Owner = "Harry"
    RG = "TestingVPC"
    Env = "Dev"
  }
}

