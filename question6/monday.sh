#!/bin/sh

# Tested on GNU bash, version 5.0.17
# with GNU coreutils 8.31
monday=$(date -d "$(date -Idate) - $(($(date +%u) - 1)) days" +"%B %d, %Y")
echo "The monday of current week is" $monday
